
package packableTime

import (
  "encoding/json"
  "time"
)

type JsonTime struct  {
  Year int
  Month int
  Day int
  Hour int
  Min int
  Sec int
  Nsec int
}

func (u JsonTime) ToJson() ([]byte, error)  {
  res, err := json.Marshal(u)
  return res, err
}

func (u *JsonTime) FromJson(buf []byte) error  {
  var res JsonTime

  err := json.Unmarshal(buf, &res)

  if err != nil  {
    return err
  }

  u.Year = res.Year
  u.Month = res.Month
  u.Day = res.Day
  u.Hour = res.Hour
  u.Min = res.Min
  u.Sec = res.Sec
  u.Nsec = res.Nsec
  return nil
}

func (u JsonTime) ToTime() time.Time  {
  return time.Date(
    u.Year,
    time.Month(u.Month),
    u.Day,
    u.Hour,
    u.Min,
    u.Sec,
    u.Nsec,
    time.UTC)
}

func (u *JsonTime) FromTime(t time.Time)  {
  u.Year = t.Year()
  u.Month = int(t.Month())
  u.Day = t.Day()
  u.Hour = t.Hour()
  u.Min = t.Minute()
  u.Sec = t.Second()
  u.Nsec = t.Nanosecond()
}

type Time struct  {
  time.Time
}

func (t Time) ToJsonTime() (ret JsonTime)  {
  ret.FromTime(t.Time)
  return
}

func (t *Time) FromJsonTime(jt JsonTime) ()  {
  *t = Time{jt.ToTime()}
}

func (t Time) ToJson() ([]byte, error)  {
  return t.ToJsonTime().ToJson()
}

func (t *Time) FromJson(buf []byte) error  {
  var ret JsonTime
  err := ret.FromJson(buf)
  if err != nil  {
    return err
  }
  t.FromJsonTime(ret)
  return nil
}

func Now() Time  {
  return Time{time.Now()}
}

